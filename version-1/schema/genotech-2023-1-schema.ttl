@prefix : <https://biomedit.ch/rdf/sphn-ontology/genotech#> .
@prefix dc: <http://purl.org/dc/elements/1.1/> .
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix genotech: <https://biomedit.ch/rdf/sphn-ontology/genotech#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix snomed: <http://snomed.info/id/> .
@prefix sphn: <https://biomedit.ch/rdf/sphn-ontology/sphn#> .
@prefix sphn-geno: <https://biomedit.ch/rdf/sphn-resource/geno/> .
@prefix sphn-hgnc: <https://biomedit.ch/rdf/sphn-resource/hgnc/> .
@prefix sphn-so: <https://biomedit.ch/rdf/sphn-resource/so/> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

<https://biomedit.ch/rdf/sphn-ontology/genotech> a owl:Ontology ;
    dc:description "The RDF schema describing concepts defined in the official Genotech dataset" ;
    dc:rights "© Copyright 2023, Personalized Health Informatics Group (PHI), SIB Swiss Institute of Bioinformatics" ;
    dc:title "The Genotech RDF Schema" ;
    dcterms:license <https://creativecommons.org/licenses/by/4.0/> ;
    owl:imports <http://snomed.info/sct/900000000000207008/version/20221231>,
        <https://biomedit.ch/rdf/sphn-ontology/sphn/2023/2>,
        <https://biomedit.ch/rdf/sphn-resource/atc/2023/1>,
        <https://biomedit.ch/rdf/sphn-resource/chop/2023/4>,
        sphn-geno:20220810,
        sphn-hgnc:20221107,
        <https://biomedit.ch/rdf/sphn-resource/icd-10-gm/2023/3>,
        <https://biomedit.ch/rdf/sphn-resource/loinc/2.74/1>,
        sphn-so:20211122,
        <https://biomedit.ch/rdf/sphn-resource/ucum/2023/1> ;
    owl:versionIRI <https://biomedit.ch/rdf/sphn-ontology/genotech/2023/1> .

genotech:hasSharedIdentifier a owl:DatatypeProperty ;
    rdfs:label "has shared identifier" ;
    rdfs:comment "identifier that is shared across data providers" ;
    rdfs:domain [ a owl:Class ;
            owl:unionOf ( sphn:SubjectPseudoIdentifier sphn:Sample ) ] ;
    rdfs:range xsd:anyURI ;
    rdfs:subPropertyOf genotech:genotechAttributeDatatype ;
    skos:definition "identifier that is shared across data providers" .

genotech:GENOTECHAttributeDatatype a owl:ObjectProperty ;
    rdfs:label "GENOTECHAttributeDatatype" ;
    rdfs:comment "GENOTECHAttributeDatatype defined by the The Genotech RDF Schema" ;
    skos:definition "GENOTECHAttributeDatatype defined by the The Genotech RDF Schema" .

genotech:GENOTECHConcept a owl:Class ;
    rdfs:label "GENOTECHConcept" ;
    rdfs:comment "GENOTECHConcept defined by the The Genotech RDF Schema" ;
    skos:definition "GENOTECHConcept defined by the The Genotech RDF Schema" .

sphn:AdministrativeCase a owl:Class ;
    rdfs:label "Administrative Case" ;
    rdfs:comment "administrative artefact for billing according to Swiss DRG" ;
    rdfs:subClassOf [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasDataProviderInstitute ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasDataProviderInstitute ] ) ],
        [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "0"^^xsd:nonNegativeInteger ;
                        owl:onProperty genotech:hasCost ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty genotech:hasCost ] ) ],
        [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "0"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasOriginLocation ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasOriginLocation ] ) ],
        [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "0"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasCareHandling ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasCareHandling ] ) ],
        [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasSubjectPseudoIdentifier ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasSubjectPseudoIdentifier ] ) ],
        [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasIdentifier ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasIdentifier ] ) ],
        [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "0"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasDischargeDateTime ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasDischargeDateTime ] ) ],
        [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasAdmissionDateTime ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasAdmissionDateTime ] ) ],
        [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "0"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasDischargeLocation ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasDischargeLocation ] ) ],
        sphn:SPHNConcept ;
    skos:definition "administrative artefact for billing according to Swiss DRG" .

sphn:Sample a owl:Class ;
    rdfs:label "Sample" ;
    rdfs:comment "any material sample for testing, diagnostic, propagation, treatment or research purposes" ;
    rdfs:subClassOf [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "0"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasFixationType ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasFixationType ] [ a owl:Restriction ;
                        owl:onProperty sphn:hasFixationType ;
                        owl:someValuesFrom sphn:Sample_fixationType ] ) ],
        [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasCollectionDateTime ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasCollectionDateTime ] ) ],
        [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "0"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasAdministrativeCase ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasAdministrativeCase ] ) ],
        [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "0"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasMaterialTypeCode ] [ a owl:Restriction ;
                        owl:onProperty sphn:hasMaterialTypeCode ;
                        owl:someValuesFrom snomed:123038009 ] ) ],
        [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasDataProviderInstitute ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasDataProviderInstitute ] ) ],
        [ a owl:Restriction ;
            owl:minCardinality "0"^^xsd:nonNegativeInteger ;
            owl:onProperty sphn:hasIdentifier ],
        [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "0"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasPrimaryContainer ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasPrimaryContainer ] [ a owl:Restriction ;
                        owl:onProperty sphn:hasPrimaryContainer ;
                        owl:someValuesFrom sphn:Sample_primaryContainer ] ) ],
        [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "0"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasBodySite ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasBodySite ] ) ],
        [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasSubjectPseudoIdentifier ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasSubjectPseudoIdentifier ] ) ],
        sphn:SPHNConcept ;
    skos:definition "any material sample for testing, diagnostic, propagation, treatment or research purposes" .

sphn:SubjectPseudoIdentifier a owl:Class ;
    rdfs:label "Subject Pseudo Identifier" ;
    rdfs:comment "a pseudo code assigned as unique identifier to an individual by a data provider institute" ;
    rdfs:subClassOf [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasDataProviderInstitute ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasDataProviderInstitute ] ) ],
        [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasIdentifier ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty sphn:hasIdentifier ] ) ],
        sphn:SPHNConcept ;
    skos:definition "a pseudo code assigned as unique identifier to an individual by a data provider institute" .

genotech:GENOTECHAttributeObject a owl:ObjectProperty ;
    rdfs:label "GENOTECHAttributeObject" ;
    rdfs:comment "GENOTECHAttributeObject defined by the The Genotech RDF Schema" ;
    skos:definition "GENOTECHAttributeObject defined by the The Genotech RDF Schema" .

genotech:hasCost a owl:ObjectProperty ;
    rdfs:label "has cost" ;
    rdfs:comment "cost of the concept" ;
    rdfs:domain sphn:AdministrativeCase ;
    rdfs:range genotech:Cost ;
    rdfs:subPropertyOf genotech:GENOTECHAttributeObject ;
    skos:definition "cost of the concept" .

genotech:hasValue a owl:DatatypeProperty ;
    rdfs:label "has value" ;
    rdfs:comment "value of the concept" ;
    rdfs:domain genotech:Cost ;
    rdfs:range xsd:double ;
    rdfs:subPropertyOf genotech:GENOTECHAttributeDatatype ;
    skos:definition "value of the concept" .

genotech:Cost a owl:Class ;
    rdfs:label "Cost" ;
    rdfs:comment "an amount that has to be paid or spent to buy or obtain something" ;
    rdfs:subClassOf [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty genotech:hasCurrencyCode ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty genotech:hasCurrencyCode ] [ a owl:Restriction ;
                        owl:onProperty genotech:hasCurrencyCode ;
                        owl:someValuesFrom sphn:Code ] ) ],
        [ a owl:Class ;
            owl:intersectionOf ( [ a owl:Restriction ;
                        owl:minCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty genotech:hasValue ] [ a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty genotech:hasValue ] ) ],
        genotech:GENOTECHConcept ;
    skos:definition "an amount that has to be paid or spent to buy or obtain something" ;
    skos:note "genotech:hasCurrencyCode allowed coding system: ISO 4217" .

genotech:hasCurrencyCode a owl:ObjectProperty ;
    rdfs:label "has currency code" ;
    rdfs:comment "code, name, coding system and version describing the currency of the concept" ;
    rdfs:domain genotech:Cost ;
    rdfs:range [ a owl:Class ;
            owl:unionOf ( sphn:Terminology sphn:Code ) ] ;
    rdfs:subPropertyOf genotech:GENOTECHAttributeObject ;
    skos:definition "code, name, coding system and version describing the currency of the concept" .

